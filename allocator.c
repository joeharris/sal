//
//  COMP1927 Assignment 1 - Memory Suballocator
//  allocator.c ... implementation
//
//  Created by Liam O'Connor on 18/07/12.
//  Modified by John Shepherd in August 2014
//  Copyright (c) 2012-2014 UNSW. All rights reserved.
//
//  Jeremy Wong and Joe Harris
//  Lab: Thurs 9am Buckland
//

#include "allocator.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define HEADER_SIZE    sizeof(struct free_list_header)
#define MAGIC_FREE     0xDEADBEEF
#define MAGIC_ALLOC    0xBEEFDEAD

#define FALSE 0
#define TRUE !FALSE

#define NULL_PREV_BLOCK -1

typedef unsigned char byte;
typedef u_int32_t vlink_t;
typedef u_int32_t vsize_t;
typedef u_int32_t vaddr_t;

typedef struct free_list_header {
   u_int32_t magic;           // ought to contain MAGIC_FREE
   vsize_t size;              // # bytes in this block (including header)
   vlink_t next;              // memory[] index of next free block
   vlink_t prev;              // memory[] index of previous free block
} free_header_t;

// pointer to a header
typedef free_header_t *Header;

// Global data
static byte *memory = NULL;   // pointer to start of suballocator memory
static vaddr_t free_list_ptr; // index in memory[] of first block in free list
static vsize_t memory_size;   // number of bytes malloc'd in memory[]

static Header initHeader (vsize_t size, int offset);
static Header getHeader (int offset);
static int exponent (int base, int power);
static int roundUp (int in);
static int getLength (void);
static void splitBlock (vlink_t offset);
static vlink_t getIndex (Header header);
static void removeBlock (Header header);
static void insertInFront (vaddr_t insertIndex, Header inFrontOf);
static void compactBlockRegion (vaddr_t startOffset);
static Header getLast (void);
static int hasAdjacentBlocks (Header blockToCompare);

void sal_init (u_int32_t size) {
   if (memory == NULL) { // do nothing if suballocator already initialised

      // Round up 'size' to a power of two, if not already
      size = roundUp(size);

      // malloc all necessary memory to begin, attach it to *memory pointer
      memory = malloc(size);

      // Abort if malloc fails, as required in spec
      if (memory == NULL) {
         fprintf (stderr, "sal_init: insufficient memory\n");
         abort ();
      }

      // Update global variables
      memory_size = size; // malloc'ed memory size
      free_list_ptr = 0; // initial free block is at position zero in memory[]

      // Create and initialise the header of the first node.
      Header first = initHeader (size, free_list_ptr);
      first->next = free_list_ptr;
      first->prev = free_list_ptr;
   }
}

// Initialise a header at the given memory offset
static Header initHeader (vsize_t size, int offset) {
   // typecasted to Header as that lets the pointer know that it points
   // to a header struct at the beginning of the memory chunk.
   Header newHeader = (Header) (memory + offset);
   newHeader->magic = MAGIC_FREE;
   newHeader->size = size;
   return newHeader;
}

void *sal_malloc (u_int32_t n) {
   // the sub-allocated memory to return to the user
   void *subStart = NULL;

   // Get the nearest 2^n above given n.
   int subSize = roundUp (n + HEADER_SIZE);

   // Loop through the free list, looking for size > subSize
   // Points initially to first free block.
   Header currHeader = getHeader (free_list_ptr);
   // Will stop at 'last' block or at found free block.
   int found = FALSE;
   do {
      // If there is a memory block either of perfect size
      // or larger so that it can be split down
      if (currHeader->size >= subSize) {
         found = TRUE;
      // Else traverse to the next free block
      } else {
         currHeader = getHeader (currHeader->next);
      }
   } while ((getIndex (currHeader) != free_list_ptr) && !found);

   // So after exiting the loop, either a block was found or
   // the end was reached without finding one.
   if (!found) {
      fprintf (stderr, "Insufficient Memory.\n");
      abort();

   // If the list will be empty after removal, return null.
   } else if (getLength () == 1 && currHeader->size == subSize) {
      subStart = NULL;

   // Allocate the found memory
   } else {
      // split down to size we need.
      while (currHeader->size > subSize) {
         splitBlock (getIndex(currHeader));
      }
      // assign the pointer to start of the memory block to return to the user.
      subStart = (Header) ((byte*)currHeader + HEADER_SIZE);

      // set this block to MAGIC_ALLOC
      currHeader->magic = MAGIC_ALLOC;

      // if the current value of free_list_ptr is to the block
      // that was just allocated then it needs to move to next
      // free one.
      if (getIndex (currHeader) == free_list_ptr) {
         free_list_ptr = currHeader->next;
      }

      // Once the block has been allocated remove it from the free list
      removeBlock (currHeader);
   }

   return subStart;
}

// Insert the block back into the free list
void sal_free (void *object) {
   // retrieve the header offset from the sub-allocated memory
   // by subtracting the header size
   vaddr_t newIndex = (vaddr_t)object - (vaddr_t)memory - HEADER_SIZE;
   Header headerToInsert = (Header) (object - HEADER_SIZE);

   Header currHeader = getHeader (free_list_ptr);
   if (headerToInsert->magic == MAGIC_ALLOC) {
      // Loop through list until you find an offset larger than
      // the offset of inserting index.

      // if new block needs to be placed last just insert it
      // otherwise search for correct placement.
      if (getIndex (getLast()) > newIndex) {
         while ((getIndex (currHeader) < newIndex) && (newIndex != 0) ) {
            currHeader = getHeader (currHeader->next);
         }

         // if the block that just got preceeded was the old
         // free_list_ptr, set it now to the newly inserted block
         if (getIndex (currHeader) == free_list_ptr) {
            free_list_ptr = newIndex;
         }
      }
      // Then insert the block before this found block
      insertInFront (newIndex, currHeader);

      // Merge contiguous free blocks around this new block
      compactBlockRegion (newIndex);
   } else {
      fprintf (stderr, "Attempt to free non-allocated memory\n");
      printf ("Magic value: %x\n", headerToInsert->magic);
      abort ();
   }
}

void sal_end (void) {
   free (memory);
}

// Print useful stats
void sal_stats (void) {
   printf ("==== sal_stats ====\n");
   printf ("memory: %p\tfree_list_ptr: %d\tmemory_size: %d\n", memory, free_list_ptr, memory_size);
   printf ("HEADER_SIZE: %lu\n", (long unsigned int) HEADER_SIZE);

   printf ("\n==== free-list stats ====\n");
   printf ("Length: %d\n", getLength());

   printf ("\n==== header info ====\n");
   printf ("free_list_ptr value: %d\n", free_list_ptr);
   Header currHeader = getHeader (free_list_ptr);

   // Will stop at 'last' block or at found free block.
   int i = 0;
   do {
      printf ("header [%d]\n", i);
      printf ("\toffset: %d\n", getIndex (currHeader));
      if (currHeader->magic == MAGIC_FREE) {
         printf ("\tmagic: MAGIC_FREE\tsize: %d\n", currHeader->size);
      } else {
         printf ("\tmagic: MAGIC_ALLOC\tsize: %d\n", currHeader->size);
      }
      printf ("\tnext: %d\tprev: %d\n", currHeader->next, currHeader->prev);
      i++;
      // Scroll across loop
      currHeader = getHeader (currHeader->next);
   } while (getIndex(currHeader) != free_list_ptr);
}

// Raises the base to the given power, returns it as int.
static int exponent (int base, int power) {
   // cumulative product starts as 1
   int product = 1;
   int i = 0;
   while (i < power) {
      product *= base;
      i++;
   }
   return product;
}

static int roundUp (int integer) {
   int n = 1;
   while (exponent (2,n) < integer) {
      n++;
   }
   integer = exponent (2,n);
   return integer;
}

// returns the length of the free list
static int getLength (void) {
   int length = 0;
   Header currHeader = getHeader (free_list_ptr);
   do {
      // Sum up the length
      length++;
      // Scroll across loop
      currHeader = getHeader (currHeader->next);
   } while (getIndex(currHeader) != free_list_ptr);
   return length;
}

static void splitBlock (vlink_t offset) {
   Header headerToSplit = getHeader (offset);

   // Create new header 'node'
   // Place in front of current header at
   // (current space + 1/2*current->size)
   // Arrange indexes next and prev for both
   // Change size value in both to previous size / 2.

   // X----
   // X--X--
   // Get half the size of the original block
   int newSize = headerToSplit->size / 2;
   // Get the position halfway offsetted across
   // the original block
   int newOffset = offset + newSize;

   // Create a new header of half the original size at this new
   // middle point.
   Header newHeader = initHeader (newSize, newOffset);
   // New block points to old original next
   newHeader->next = headerToSplit->next;
   // New block points back to the original block
   newHeader->prev = offset;
   // Need to set block in front to point back to the new block.
   getHeader (newHeader->next)->prev = newOffset;

   // Original block is now set to half the original size
   headerToSplit->size = newSize;
   // Now set to point to the new block in front of it
   headerToSplit->next = newOffset;
}

// Returns a pointer to the header at the given offset.
static Header getHeader (int offset) {
   // gets the header situated at the offset value
   // takes address of offsetted block and typecasts
   // to header pointer.
   Header header = (Header)(&memory[offset]);
   return header;
}

// Returns the index/offset of a given header
static vlink_t getIndex (Header header) {
   return getHeader (header->next)->prev;
}

// Removes a block header from the free-list
// when it has been allocated to the user
static void removeBlock (Header header) {
   Header headerToRemove = header;
   // Set prev to point after it
   // Set next to point before it
   getHeader (headerToRemove->prev)->next = headerToRemove->next;
   getHeader (headerToRemove->next)->prev = headerToRemove->prev;
}

// Inserts a block header into the free list.
static void insertInFront (vaddr_t insertIndex, Header inFrontOf) {
   Header insert = getHeader (insertIndex);
   insert->next = getIndex (inFrontOf);
   insert->prev = inFrontOf->prev;
   insert->magic = MAGIC_FREE;
   getHeader (inFrontOf->prev)->next = insertIndex;

   //inFrontOf->next = insertIndex;
   inFrontOf->prev = insertIndex;
}

// search around the given block, merging adjacent free blocks where required
static void compactBlockRegion (vaddr_t searchOffset) {

   // the resulting block after the merge operation
   vaddr_t resultOffset = searchOffset;

   Header currHeader = getHeader (searchOffset);
   if (currHeader->magic != MAGIC_FREE) {
      fprintf(stderr, "Memory corruption, allocated memory in free-list.\n");
      abort();
   }

   // check the previous block: if one exists && is adjacent && of equal size
   if (currHeader->prev < searchOffset && hasAdjacentBlocks (currHeader)
      && getHeader (currHeader->prev)->size == currHeader->size) {

      Header prevHeader = getHeader (currHeader->prev);

      prevHeader->size = prevHeader->size + currHeader->size; // Extend its the size

      // Update pointers:
      // Point this block forwards to the next
      prevHeader->next = currHeader->next;
      // Point the next block backwards to this block
      getHeader (currHeader->next)->prev = getIndex (prevHeader);

      // Destroy the current block
      removeBlock (currHeader);

      // Resume loop from the resulting block
      resultOffset = getIndex (prevHeader);

      compactBlockRegion (resultOffset);

   // check the next block: if one exists && is adjacent && of equal size
   } else if (currHeader->next > searchOffset && hasAdjacentBlocks (currHeader)
      && getHeader (currHeader->next)->size == currHeader->size) {

      Header nextHeader = getHeader (currHeader->next);

      currHeader->size = currHeader->size + nextHeader->size; // Extend its the size

      // Update pointers:
      // Point this block forwards to the next
      currHeader->next = nextHeader->next;
      // Point the next block backwards to this block
      getHeader (nextHeader->next)->prev = getIndex (currHeader);

      // Destroy the current block
      removeBlock (nextHeader);

      compactBlockRegion (resultOffset);
   }
}

static Header getLast (void) {
   return getHeader (getHeader (free_list_ptr)->prev);
}

// determine if there is a directly adjacent free block
// either directly behind, or in front of the given block.
static int hasAdjacentBlocks (Header blockToCompare) {
   int hasAdjacentBlocks = FALSE;

   // calculate the end boundaries of the previous block
   int endBoundaryPrevious = getHeader (blockToCompare->prev)->size + blockToCompare->prev;
   // calculate the end boundaries of this block
   int endBoundaryCurrent = getIndex (blockToCompare) + blockToCompare->size;

   // if there is an adjoining block before
   if (endBoundaryPrevious == getIndex (blockToCompare)) {
      hasAdjacentBlocks = TRUE;

   // or if there is an adjoining block after
   } else if (endBoundaryCurrent == blockToCompare->next) {
      hasAdjacentBlocks = TRUE;
   }

   return hasAdjacentBlocks;
}
