# README #

### What is this repository for? ###

* A memory [s]ub [al]locator. Working similar to Malloc or Realloc it will malloc a large region of memory then reallocate it to you as you request it.
* Built for a university project.